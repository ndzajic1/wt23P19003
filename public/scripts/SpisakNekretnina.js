let SpisakNekretnina = function () {

    let listaNekretnina = [];
    let listaKorisnika = [];

    let init = function (pListaNekretnina, pListaKorisnika) {
        listaNekretnina = pListaNekretnina;
        listaKorisnika = pListaKorisnika;
    }
    let filtrirajNekretnine = function (kriterij) {
        let filtrirano = [...listaNekretnina];
        for(let svojstvo in kriterij){
            switch(svojstvo){
                case 'tip_nekretnine':
                    filtrirano = filtrirano.filter(n => n.tip_nekretnine == kriterij.tip_nekretnine);
                    break;
                case 'min_kvadratura':
                    filtrirano = filtrirano.filter(n => n.kvadratura >= kriterij.min_kvadratura);
                    break;
                case 'max_kvadratura':
                    filtrirano = filtrirano.filter(n => n.kvadratura <= kriterij.max_kvadratura);
                    break;
                case 'min_cijena':
                    filtrirano = filtrirano.filter(n => n.cijena >= kriterij.min_cijena);
                    break;
                case 'max_cijena':
                    filtrirano = filtrirano.filter(n => n.cijena <= kriterij.max_cijena);
                    break;
            }
        }
        return filtrirano;
    }
    let ucitajDetaljeNekretnine = function (id) {
        let ucitana = listaNekretnina.filter(n => n.id == id);
        if(ucitana === undefined)
            return null;
        return ucitana;
    }
    return {
        init: init,
        filtrirajNekretnine: filtrirajNekretnine,
        ucitajDetaljeNekretnine: ucitajDetaljeNekretnine
    }
};