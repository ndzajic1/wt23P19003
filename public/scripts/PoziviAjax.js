
const PoziviAjax = (() => {
    // fnCallback se u svim metodama poziva kada stigne
    // odgovor sa servera putem Ajax-a
    // svaki callback kao parametre ima error i data,
    // error je null ako je status 200 i data je tijelo odgovora
    // ako postoji greška, poruka se prosljeđuje u error parametru
    // callback-a, a data je tada null

    // vraća korisnika koji je trenutno prijavljen na sistem
    function impl_getKorisnik(fnCallback) {
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200)
            fnCallback(null, JSON.parse(ajax.responseText));
        else
            fnCallback({"greska": true}, null);
        }
        ajax.open("GET", `/korisnik`, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();

    }
    
    // ažurira podatke loginovanog korisnika
    function impl_putKorisnik(noviPodaci, fnCallback) {
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200)
            fnCallback(null, JSON.parse(ajax.responseText));
        else
            fnCallback({"greska": true}, null);
        }
        ajax.open("PUT", `/korisnik`, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(noviPodaci));

        }

    // dodaje novi upit za trenutno loginovanog korisnika
    function impl_postUpit(nekretnina_id, tekst_upita, fnCallback) {
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200)
            fnCallback(null, JSON.parse(ajax.responseText));
        else
            fnCallback({"greska": true}, null);
        }
        ajax.open("POST", `/upit`, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify({"nekretnina_id":nekretnina_id, "tekst_upita":tekst_upita}));
    }
    function impl_getNekretnine(fnCallback) {
            let ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                fnCallback(null, JSON.parse(ajax.responseText));
            else
                fnCallback({"greska": true}, null);
            }         
            ajax.open("PUT", `/nekretnine`, true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send();
        }

    function impl_postLogin(username, password, fnCallback) {
            let ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                console.log(ajax.responseText);
                if (ajax.readyState == 4 && ajax.status == 200)
                    fnCallback(null,JSON.parse(ajax.responseText));
                else{
                    fnCallback({"greska": true}, null);
                }
            }
            ajax.open("POST", `/login`, true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify({"username": username, "password": password}));  
        }
        function impl_postLogout(fnCallback) {
            let ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                fnCallback(null, JSON.parse(ajax.responseText));
            else   
                fnCallback({"greska": true}, null);   
            }
            ajax.open("POST", `/logout`, true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send();
        }

        function impl_isLogiran(fnCallback){
            let ajax = new XMLHttpRequest();
            
            ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null, JSON.parse(ajax.responseText));
            }
            else   
                fnCallback({"greska": true}, null);   
            }
            ajax.open("GET", `/logiran`, true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send();
        }

        function impl_getNekretninaById(nekretnina_id, fnCallback) {
            let ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                fnCallback(null, JSON.parse(ajax.responseText));
            else   
                fnCallback({"greska": true}, null);   
            }
            ajax.open("GET", `/nekretnina/${nekretnina_id}`, true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(); 
            
        }

        return {
            postLogin: impl_postLogin,
            postLogout: impl_postLogout,
            getKorisnik: impl_getKorisnik,putKorisnik: impl_putKorisnik,
            postUpit: impl_postUpit,
            getNekretnine: impl_getNekretnine,
            isLogiran: impl_isLogiran,
            getNekretninaById: impl_getNekretninaById
        };
        })();