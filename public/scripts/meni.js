function ucitajMeni() {
    // 
    PoziviAjax.isLogiran(function (err, data) {
        if (!err) {
            document.getElementById('profil').innerHTML = 'Profil';
            let prijava = document.getElementById('prijava');
            prijava.innerHTML = "Odjava";
            //prijava.removeAttribute('href');
            prijava.onclick = odjava;
        }
        else {
            document.getElementById('profil').innerHTML = '';
            let prijava = document.getElementById('prijava');
            //prijava.href = "prijava.html";
            prijava.innerHTML = "Prijava";
        }

    });
}

function odjava() {
    PoziviAjax.postLogout(function (err, data) {
        if (err) console.log(err);
        document.getElementById('profil').innerHTML = '';
    });

    return true;
}