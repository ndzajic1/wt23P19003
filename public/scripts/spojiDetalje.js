function iscrtajUpit(u){
    let upit = document.createElement('li');
    upit.id = u.id;
    upit.className = "upit";
    upit.innerHTML = `<p><strong>${u.username}</strong></p><p>${u.tekst_upita}</p>`
    document.getElementById('upiti-lista').appendChild(upit);
}

let id = localStorage.getItem("detaljiID");

PoziviAjax.getNekretninaById(id, function(err, data){
    if(data){
        document.getElementById("naziv").innerHTML = data.naziv;
        document.getElementById("kvadratura").innerHTML = data.kvadratura;
        document.getElementById("cijena").innerHTML = data.cijena;
        document.getElementById("opis").innerHTML = data.opis;
        document.getElementById("tip_grijanja").innerHTML = data.tip_grijanja;
        document.getElementById("datum_objave").innerHTML = new Date(data.datum_objave).toLocaleDateString('en-GB');
        document.getElementById("lokacija").innerHTML = data.lokacija;
        document.getElementById("godina_izgradnje").innerHTML = data.godina_izgradnje;
        let upiti = data.upiti;
        for(let u of upiti)
            iscrtajUpit(u);
    }
});

