
const MarketingAjax = (() => {

    let nizNekretnina = [];
    let promjena = true;
    let azuriraniPodaci = [];


    function osvjeziPretrage(divNekretnine) {
        let tijelo = {};
        if(promjena){
            // uzmiNekretnine(divNekretnine);
            tijelo.nizNekretnina = nizNekretnina;
            
            promjena = false;
        }
       // console.log("Tijelo: ", tijelo)
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                
                azuriraniPodaci = JSON.parse(ajax.responseText).nizNekretnina;
                // ovdje su novi, sad ih samo treba ubaciti na frontend, a to neka radi osvjeziKlikove
                osvjeziKlikove(divNekretnine);
            }            
            else{
                console.log(ajax.responseText);
            }
        }; 
        ajax.open("POST", `/marketing/osvjezi`, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        //console.log("tijelo klijent ", tijelo, tijelo.nizNekretnina);
        if(!tijelo.nizNekretnina)
            ajax.send();
        else
            ajax.send(JSON.stringify(tijelo));
    }

    function osvjeziKlikove(divNekretnine) {
        
        if(azuriraniPodaci.length != 0)
                console.log("[tg]", azuriraniPodaci);
        azuriraniPodaci.forEach(item => {
            let pretrageDiv = document.getElementById(`pretrage-${item.id}`);
            let klikoviDiv = document.getElementById(`klikovi-${item.id}`);
            pretrageDiv.innerHTML = item.pretrage; 
            klikoviDiv.innerHTML = item.klikovi; 
            
        });
    }

    function novoFiltriranje(listaFiltriranihNekretnina) {
        // poslati zahtjev na marketing/nekretnine o rezultatima filtriranja, i jos pomoci osvjezavanje
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200){
           console.log(ajax.responseText);
        }            
        else{
            console.log(ajax.responseText);
        }
        };
        nizNekretnina = listaFiltriranihNekretnina.map(n => n.id);
       // console.log("Market filter:\n", nizNekretnina);
        promjena = true;
        // send req
        ajax.open("POST", `/marketing/nekretnine`, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify({"nizNekretnina":nizNekretnina}));
        
    }

    function klikNekretnina(idNekretnine) {
        // post zahtjev za klik, uvecanje itema i stopiranje osvjezavanja
        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200){
            console.log(ajax.responseText);
        }            
        else{
            console.log(ajax.responseText);
        }
        };
        ajax.open("POST", `/marketing/nekretnina/${idNekretnine}`, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();
        
        
        


        nizNekretnina = [idNekretnine];
        promjena = true;
        
    }

    return {
        osvjeziPretrage: osvjeziPretrage,
        osvjeziKlikove: osvjeziKlikove,
        novoFiltriranje: novoFiltriranje,
        klikNekretnina: klikNekretnina
    };

})();