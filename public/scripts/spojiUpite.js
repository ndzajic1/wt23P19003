PoziviAjax.isLogiran(function(err,data){  
    if(!data){
        document.getElementById("noviUpit").style.display = "none";
    }
    else{
        document.getElementById("noviUpit").style.display = "block";
        document.getElementById("dodajUpitButton").addEventListener('click', function () {
            let nid = localStorage.getItem("detaljiID"),
                tekst = document.getElementById("tekstUpita").value;
            PoziviAjax.postUpit(nid, tekst, function(err,data){
                if(data){
                    refreshUpite();
                }   
            });
        });
    }
});

function refreshUpite(){
    document.getElementById("upiti-lista").innerHTML = "";
    PoziviAjax.getNekretninaById(id, function(err, data){
        if(data){
            let upiti = data.upiti;
            for(let u of upiti)
                iscrtajUpit(u);
           // kreirajFormu();
        }
    });
}

function iscrtajUpit(u){
    let upit = document.createElement('li');
    upit.id = u.id;
    upit.className = "upit";
    upit.innerHTML = `<p><strong>${u.username}</strong></p><p>${u.tekst_upita}</p>`
    document.getElementById('upiti-lista').appendChild(upit);
}


