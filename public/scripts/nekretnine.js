
//kljuc za povezivanje stila i slike na osnovu tipa nekretnine
function kljuc(tip) {
    switch (tip) {
        case 'Stan':
            return 'stan';
        case 'Kuća':
            return 'kuca';
        case 'Poslovni prostor':
            return 'pp';
        default:
            return null;
    }
}

function iscrtajNekretninu(nekretnina, divParent) {
    let item = document.createElement('div');
    item.id = `nekretnina-${nekretnina.id}`;
    item.classList.add('item', `${kljuc(nekretnina.tip_nekretnine)}-item`);
    // slika
    let pic = document.createElement('img');
    pic.src = `../img/${kljuc(nekretnina.tip_nekretnine)}-sa.jpg`;
    pic.alt = nekretnina.naziv;
    item.appendChild(pic);
    // naslov
    let naslov = document.createElement('p');
    naslov.appendChild(document.createTextNode(nekretnina.naziv));
    naslov.style.textAlign = "left";
    item.appendChild(naslov);
    // kvadratura
    let kvadrata = document.createElement('p');
    kvadrata.appendChild(document.createTextNode(nekretnina.kvadratura));
    kvadrata.style.textAlign = "left";
    item.appendChild(kvadrata);
    // cijena
    let cijena = document.createElement('p');
    cijena.appendChild(document.createTextNode(nekretnina.cijena));
    cijena.style.textAlign = "right";
    item.appendChild(cijena);
    //marketing div
    let marketingDiv = document.createElement('div');
    marketingDiv.id = `marketing-${nekretnina.id}`;
    ////// pretrage div
    let pretrageDiv = document.createElement('div');
    pretrageDiv.style.display = "flex";
    let pretrage = document.createElement('div');
    pretrage.id = `pretrage-${nekretnina.id}`;
    pretrage.innerHTML = '';
    let pretrageLabel = document.createElement('div');
    pretrageLabel.innerHTML = 'pretraga';
    pretrageLabel.style.marginLeft = "10%";
    pretrageDiv.appendChild(pretrage);
    pretrageDiv.appendChild(pretrageLabel);
    let klikoviDiv = document.createElement('div');
    klikoviDiv.style.display = "flex";
    let klikovi = document.createElement('div');
    klikovi.innerHTML = '';
    klikovi.id = `klikovi-${nekretnina.id}`;
    let klikoviLabel = document.createElement('div');
    klikoviLabel.innerHTML = 'klikova';
    klikoviLabel.style.marginLeft = "10%";
    klikoviDiv.appendChild(klikovi);
    klikoviDiv.appendChild(klikoviLabel);
    marketingDiv.appendChild(pretrageDiv);
    marketingDiv.appendChild(klikoviDiv);
    item.appendChild(marketingDiv);
    // button div
    let buttonDiv = document.createElement('div');
    buttonDiv.className = 'detalji-div';
    //button
    let button = document.createElement('button');
    button.appendChild(document.createTextNode('Detalji'));
    button.className = 'detalji-button';
    button.addEventListener('click', function () {
        let id = nekretnina.id;
        klikDetalji(id);
    });
    buttonDiv.appendChild(button);
    item.appendChild(buttonDiv);
    // dodatni 
    let extraDiv = document.createElement('div');
    extraDiv.id = `extra-${nekretnina.id}`;
    let lokacija = document.createElement('div');
    lokacija.innerHTML = nekretnina.lokacija;
    lokacija.style.marginBottom = "4px";
    extraDiv.appendChild(lokacija);
    let godina = document.createElement('div');
    godina.innerHTML = nekretnina.godina_izgradnje;
    godina.style.marginBottom = "4px";
    extraDiv.appendChild(godina);
    // novo dugme
    let noviButton = document.createElement('button');
    noviButton.appendChild(document.createTextNode('Otvori detalje'));
    noviButton.addEventListener('click', function () {
        window.location.href = `/detalji.html`
    });
    extraDiv.appendChild(noviButton);
    // final
    item.appendChild(extraDiv);
    extraDiv.style.display = "none";
    // item u grid
    divParent.appendChild(item);
}
function spojiNekretnine(divReferenca, instancaModula, tip_nekretnine, kriterij) {
    // pozivanje metode za filtriranje
    let kr = kriterij;
    kr.tip_nekretnine = tip_nekretnine;
    let filtrirane = instancaModula.filtrirajNekretnine(kr);
    //  console.log(filtrirane);
    // iscrtavanje elemenata u divReferenca element
    for (let nekretnina of filtrirane) {
        if (nekretnina.tip_nekretnine == 'Stan')
            iscrtajNekretninu(nekretnina, divStan);
        else if (nekretnina.tip_nekretnine == 'Kuća')
            iscrtajNekretninu(nekretnina, divKuca);
        else
            iscrtajNekretninu(nekretnina, divPp);
    }

}

let selektovanaID = null;

function klikDetalji(id) {
    // alert(id);
    MarketingAjax.klikNekretnina(id);
    if (selektovanaID) {
        let extraDiv = document.getElementById(`extra-${selektovanaID}`);
        extraDiv.style.display = 'none';
        let refDiv = document.getElementById(`nekretnina-${selektovanaID}`);
        refDiv.classList.remove('selektovana');
    }
    let item = document.getElementById(`nekretnina-${id}`);
    // stil nekako ustimati npr
    item.classList.add('selektovana');
    document.getElementById(`extra-${id}`).style.display = '';
    selektovanaID = id;
    localStorage.setItem('detaljiID', id);

}
function uzmiKriterij() {
    let min_cijena = document.getElementById('min_cijena').value,
        max_cijena = document.getElementById('max_cijena').value,
        min_kvadratura = document.getElementById('min_kvadratura').value,
        max_kvadratura = document.getElementById('max_kvadratura').value;
    let kriterij = {};
    if (min_cijena) kriterij.min_cijena = parseFloat(min_cijena);
    if (max_cijena) kriterij.max_cijena = parseFloat(max_cijena);
    if (min_kvadratura) kriterij.min_kvadratura = parseFloat(min_kvadratura);
    if (max_kvadratura) kriterij.max_kvadratura = parseFloat(max_kvadratura);
    return kriterij;
}
function filtriranje() {
    let kriterij = uzmiKriterij();
    //   console.log(kriterij);

    let filtrirane = nekretnine.filtrirajNekretnine(kriterij);

    MarketingAjax.novoFiltriranje(filtrirane);

    divStan.innerHTML = '';
    spojiNekretnine(divStan, nekretnine, "Stan", kriterij);
    divKuca.innerHTML = '';
    spojiNekretnine(divKuca, nekretnine, "Kuća", kriterij);
    divPp.innerHTML = '';
    spojiNekretnine(divPp, nekretnine, "Poslovni prostor", kriterij);

    selektovanaID = null;
    MarketingAjax.osvjeziPretrage(divBody);
    // document.getElementById(`nekretnina-${selektovanaID}`).classList.remove('selektovana');

}
const divBody = document.getElementById("nekretnine");
const divStan = document.getElementById("stan");
const divKuca = document.getElementById("kuca");
const divPp = document.getElementById("pp");
let nekretnine = SpisakNekretnina();

// ne treba ova lista uopšte
let listaNekretnina = []
let listaKorisnika = []



let ajax = new XMLHttpRequest();
ajax.onreadystatechange = function () {
    if (ajax.readyState == 4 && ajax.status == 200) {
        //  console.log(ajax.responseText);
        // dobijena lista
        listaNekretnina = JSON.parse(ajax.responseText);
        nekretnine.init(listaNekretnina, listaKorisnika);
        // console.log("Nekretnine:\n", listaNekretnina);
        // trazimo podatke od servera  za njih

        MarketingAjax.novoFiltriranje(listaNekretnina);
        MarketingAjax.osvjeziPretrage(divBody);
        // MarketingAjax.osvjeziPretrage(divBody);
        spojiNekretnine(divStan, nekretnine, "Stan", {});
        spojiNekretnine(divKuca, nekretnine, "Kuća", {});
        spojiNekretnine(divPp, nekretnine, "Poslovni prostor", {});

        setInterval(() => MarketingAjax.osvjeziPretrage(divBody), 500);
        //MarketingAjax.osvjeziPretrage(divBody);
        //MarketingAjax.osvjeziPretrage(divBody);
        //MarketingAjax.osvjeziPretrage(divBody);


    }
    else {

    }
};
ajax.open("GET", `/nekretnine`);
ajax.setRequestHeader("Content-Type", "application/json");
ajax.send();




