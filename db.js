const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    "DBWT19",
    `${process.env.MYSQL_USER}`,
    `${process.env.MYSQL_PASSWORD}`,
    {
      host: `${process.env.MYSQL_DB_HOST}`,
      dialect: "mysql",
      port: "3306",
      logging: false,
    }
  );
  
 
const db = {};
const fs = require('fs');
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.nekretnina =require(__dirname + '/nekretnina.js')(sequelize);
db.korisnik = require(__dirname + '/korisnik.js')(sequelize);
db.upit = require(__dirname + '/upit.js')(sequelize);

db.korisnik.hasMany(db.upit, { as: 'recenzijeKorisnika' });
db.nekretnina.hasMany(db.upit, { as: 'recenzijeNekretnine' });

// punjenje baze iz data foldera 
function initialize() {
    fs.readFile('data/korisnici.json', function (err, buff) {
        let data = JSON.parse(buff.toString());
        db.korisnik.bulkCreate(data, { ignoreDuplicates: true }).then(function (us) {
            fs.readFile('data/nekretnine.json', function (err, buff) {
                let data = JSON.parse(buff.toString());
                fs.readFile('data/marketing.json', function (err, buff) {
                    let market = JSON.parse(buff.toString());
                    let upiti = [];
                    for (let i = 0; i < data.length; i++) {
                        data[i].klikovi = market[i].klikovi;
                        data[i].pretrage = market[i].pretrage;
                        oviUpiti = data[i].upiti
                        for (let u of oviUpiti) {
                            u.NekretninaId = data[i].id
                            u.KorisnikId = u.korisnik_id
                            delete u.korisnik_id
                        }
                        delete data[i].upiti;
                        upiti.push(...oviUpiti)
                    }
                    db.nekretnina.bulkCreate(data, { ignoreDuplicates: true }).then(function (n) {
                        db.upit.bulkCreate(upiti, { ignoreDuplicates: true }).then(function (u) {
                            console.log("Baza napunjena")
                        }).catch(function (err) {
                            console.log("Greska pri dodavanju upita: ", err);
                        })
                    }).catch(function (err) {
                        console.log("Greska pri dodavanju nekretnine ", err);
                    });

                });
            });
        }).catch(function (err) {
            console.log("Greska pri dodavanju korisnika: ", err);
        });
    });

}



sequelize.sync().then(function(x){
    initialize()
});

module.exports = db;

