const express = require('express');
const session = require("express-session");
const path = require('path');
const fs = require('fs');
const bcrypt = require('bcrypt');
// comment nothing
const db = require('./db.js')

const { Op } = require('sequelize');

const app = express();

const publicPath = path.join(__dirname, 'public')

// obtain ip address
const ip = process.env.EC2_PUBLIC_IP
const content = `localStorage.setItem('ip',${ip})`
const filePath = path.join(publicPath, 'scripts', 'ec2_ip.js')
fs.writeFileSync(filePath, content, 'utf8');
//done

app.use(express.json())
app.use(express.static(publicPath));

app.use(session({
    secret: 'spirala3',
    resave: true,
    saveUninitialized: true
}));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'html', `nekretnine.html`));
});

app.get('/meni.html', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'html', `meni.html`));
});
app.get('/prijava.html', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'html', `prijava.html`));
});
app.get('/profil.html', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'html', `profil.html`));
});
app.get('/detalji.html', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'html', `detalji.html`));
});
app.get('/nekretnine.html', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'html', `nekretnine.html`));
});

app.post('/login', function (req, res) {
    try {
        if (req.session.user == null) {
            let _username = req.body.username;
            db.korisnik.findOne({ where: { username: `${_username}` } }).then(function (u) {
                if (!u)
                    res.status(401).json({ "poruka": "Neuspješna prijava" });
                else {
                    bcrypt.compare(req.body.password, u.password, function (err, passwordCorrect) {
                        console.log(passwordCorrect);
                        if (err) throw err;
                        else if (passwordCorrect) {
                            req.session.user = u.username;
                            res.status(200).json({ "poruka": "Uspješna prijava" });
                        }
                        else {
                            res.status(401).json({ "poruka": "Neuspješna prijava" });
                        }
                    });
                }
            }).catch(function (err) {
                console.log(err);
            });
        }
        else {
            res.status(401).json({ "poruka": "Neuspješna prijava" });
        }
    } catch (err) {
        console.log(err);
        res.status(401).json({ "poruka": "Neuspješna prijava" });
    }
});

app.post('/logout', function (req, res) {
    try {
        if (req.session.user == null) {
            res.status(401).json({ "greska": "Neautorizovan pristup" });
        }
        else {
            req.session.user = null;
            res.status(200).json({ "poruka": "Uspješno ste se odjavili" });
        }
    } catch (err) {
        console.log(err);
        res.status(401).json({ "greska": "Neautorizovan pristup" });
    }
});

// pomocna, da se utvrdi da li sesija pripada registrovanom korisniku
app.get('/logiran', function (req, res) {
    try {
        if (req.session.user == null) {
            res.status(401).json({ "status": false });
        }
        else {
            res.status(200).json({ "status": true });
        }
    } catch (err) {
        console.log(err);
        res.status(401).json({ "status": false });
    }
});

app.get('/korisnik', function (req, res) {
    try {
        if (req.session.user == null) {
            res.status(401).json({ "greska": "Neautorizovan pristup" });
        }
        else {
            let _username = req.session.user;
            db.korisnik.findOne({ where: { username: _username } }).then(function (user) {
                console.log("Pronadjen user: ", user);
                res.status(200).json(user);
            }).catch(function (err) {
                console.log("Greska s bazom: ", err);
                res.status(401).json({ "greska": "Neautorizovan pristup" });
            })
        }
    } catch (err) {
        console.log(err);
    }
});

app.put('/korisnik', function (req, res) {
    try {
        if (req.session.user == null) { //|| (req.body.username && req.session.user != req.body.username)) {
            res.status(401).json({ "greska": "Neautorizovan pristup" });
        }
        else {
            let novo = req.body;
            let user = req.session.user;
            if (req.body.password) {
                bcrypt.hash(req.body.password, 10, function (err, hash) {
                    novo.password = hash;
                    db.korisnik.update(novo, { where: { username: user } }).then(function (user) {
                        console.log("Azuriran: ", user);
                        res.status(200).json({ "poruka": "Podaci su uspješno ažurirani" });
                    }).catch(function (err) {
                        console.log("Greska pri azuriranju: ", err);
                    });
                });
            }
            else {
                db.korisnik.update(novo, { where: { username: user } }).then(function (user) {
                    console.log("Azuriran: ", user);
                    res.status(200).json({ "poruka": "Podaci su uspješno ažurirani" });
                }).catch(function (err) {
                    console.log("Greska pri azuriranju: ", err);
                });
            }
        }
    } catch (err) {
        console.log(err);
        res.status(401).json({ "greska": "Neautorizovan pristup" });
    }
});

app.get("/nekretnine", function (req, res, next) {
    try {
        db.nekretnina.findAll().then(function (nekr) {
            res.status(200).json(nekr);
        }).catch(function (err) {
            console.log(err);
            res.sendStatus(401);
        });
    } catch (err) {
        console.log(err);
        res.sendStatus(401);
    }
});

app.post('/upit', function (req, res) {
    try {
        if (req.session.user == null) {
            res.status(401).json({ "greska": "Neautorizovan pristup" });
        }
        else {
            console.log("Ovdje ", req.body)
            let nekretnina_id = req.body.nekretnina_id;
            let tekst_upita = req.body.tekst_upita;
            let user = req.session.user;
            
            db.nekretnina.findOne({ where: { id: nekretnina_id } }).then(function (ident) {
                if (!ident) {
                    res.status(400).json({ "greska": `Nekretnina sa id-em ${nekretnina_id} ne postoji` });
                }
                else {
                    db.korisnik.findOne({ where: { username: user } }).then(function (user) {
                        let id = user.id;
                        let upit = {};
                        upit.KorisnikId = id;
                        upit.NekretninaId = nekretnina_id;
                        upit.tekst_upita = tekst_upita;
                        db.upit.create(upit).then(function (u) {
                            res.status(200).json({ "poruka": "Upit je uspješno dodan" });
                        }).catch(function (err) {
                            console.log(err);
                            res.sendStatus(401);
                        });
                    }).catch(function (err) {
                        console.log(err);
                        res.sendStatus(401);
                    });
                }
            }).catch(function (err) {
                console.log(err);
                res.sendStatus(401);
            });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(401);
    }
});

let promjeneZaUsera = new Map(); // (sessionID, {"niz": [], "izmijenjene": []})

function azurirajPromjeneZaUsera(niz) {
    niz.forEach(id => {
        promjeneZaUsera.forEach(function (val, key, pair) {
            if (val.niz.includes(id)) {

                val.izmijenjene.push(id);
            }
        });
    });

}


app.post('/marketing/nekretnine', function (req, res) {
    try {
        // nova verzija
        let prviPut = !promjeneZaUsera.has(req.sessionID);
        let niz = req.body.nizNekretnina;
        db.nekretnina.increment('pretrage', { where: { id: { [Op.or]: niz } } }).then(function (n) {
            if (!prviPut) {
                azurirajPromjeneZaUsera(niz);
            }
            res.sendStatus(200);
        }).catch(function (err) {
            console.log(err);
            res.sendStatus(400);
        });


    } catch (err) {
        console.log(err);
        res.sendStatus(400);
    }

});

app.post('/marketing/nekretnina/:id', function (req, res) {
    try {
        // nova verzija
        let nid = parseInt(req.params.id, 10);
        db.nekretnina.increment('klikovi', { where: { id: nid } }).then(function (n) {
            azurirajPromjeneZaUsera([nid]);
            res.sendStatus(200);
        }).catch(function (err) {
            console.log(err);
            res.sendStatus(400);
        });

    } catch (err) {
        console.log(err);
        res.sendStatus(400);
    }
});

app.post('/marketing/osvjezi', function (req, res) {

    try {

        if (req.get('Content-Length') == 0) {
            let izmjene = promjeneZaUsera.get(req.sessionID).izmijenjene;
            if (izmjene && izmjene.length != 0) {
                // nova 
                db.nekretnina.findAll({ attributes: ['id', 'pretrage', 'klikovi'], where: { id: { [Op.or]: izmjene } } }).then(function (target) {
                    promjeneZaUsera.get(req.sessionID).izmijenjene = [];
                    res.status(200).json({ "nizNekretnina": target });
                }).catch(function (err) {
                    res.status(200).json({ "nizNekretnina": [] });
                });

            }
            else {
                res.status(200).json({ "nizNekretnina": [] });
            }
        }
        else {
            let niz = req.body.nizNekretnina;
            if (!promjeneZaUsera.has(req.sessionID)) {
                promjeneZaUsera.set(req.sessionID, { "niz": niz, "izmijenjene": niz });
            }
            req.session.nizNekretnina = niz;

            db.nekretnina.findAll({ attributes: ['id', 'pretrage', 'klikovi'], where: { id: { [Op.or]: niz } } }).then(function (target) {
                promjeneZaUsera.set(req.sessionID, { "niz": niz, "izmijenjene": niz });
                azurirajPromjeneZaUsera(niz);
                res.status(200).json({ "nizNekretnina": target });
            }).catch(function (err) {
                res.status(200).json({ "nizNekretnina": [] });
            });

        }
    } catch (err) {
        console.log(err);
        res.sendStatus(400);
    }

});

app.get('/nekretnina/:id', function (req, res) {
    try {
        let id = parseInt(req.params.id, 10);
        db.nekretnina.findByPk(id).then(function (n) {
            if (!n) {
                res.status(400).json({ "greska": `Nekretnina sa id-em ${id} ne postoji` });
            }
            else {
                let detalji = n.dataValues;
                // sada pokupiti i upite
                db.upit.findAll({ where: { NekretninaId: id } }).then(function (upiti) {
                    let modUpiti = upiti.map(u => u.dataValues)
                    let idNiz = modUpiti.map(u => u.KorisnikId);
                    db.korisnik.findAll({ attributes: ['id', 'username'], where: { id: { [Op.or]: idNiz } } }).then(function(users){
                        users = users.map(u => u.dataValues);
                        console.log(users);
                        let mapa = new Map()
                        for(let u of users)
                            mapa.set(u.id,u.username);
                        for(let upit of modUpiti){
                            upit.username = mapa.get(upit.KorisnikId);
                        }
                        console.log(modUpiti)
                        detalji.upiti = modUpiti;
                        res.status(200).json(detalji);

                    }).catch(function (err) {
                        console.log(err);
                    });
                }).catch(function (err) {
                    console.log(err);
                })

            }
        }).catch(function (err) {
            console.log(err);
        })
    } catch (err) {
        console.log(err);
    }

});


app.listen(8080);




